const { inspect } = require("util");

// # List
//
// A list implementation in form of a recursive data structure.
//
// ## Construction
//
// The empty list is known as `empty`

const empty = decorate(function list(emptyF) {
  return emptyF();
});

// Given a list (like for example `empty`), a _bigger_ list can be constructed by inserting an element up first. The `cons` _constructor_ is to be called in this case:
//
// ```
// const myList = cons(1, cons(2, empty))
// const alternative = cons(1, cons(2))
// ```

function cons(first, rest = empty) {
  return decorate(function list(_, consF) {
    return consF(first, rest);
  });
}

// Per default, JavaScript is not very helpful in providing meaningful String representations
// We need to provide them manually

function decorate(list) {
  list.toString = toString;
  list.inspect = toString;
  list[inspect.custom] = toString;
  return list;
}

function toString() {
  return this(
    () => ":",
    (first, rest) => `(${first}):${rest}`
  );
}

// ## Querying
//
// One may use `isEmpty` to check, whether a given list is _empty_:

function isEmpty(list) {
  return list(
    () => true,
    () => false
  );
}

// To de-construct a _non-empty_ list, there are two functions `first` and `rest`, which provide us with the _first_ element, and the _rest_ of the given list. Calling this functions on an _empty_ list will result in an `Error` being thrown.

function first(list) {
  return list(throwEmpty, (first) => first);
}

function rest(list) {
  return list(throwEmpty, (_, rest) => rest);
}

// Using an empty list is forbidden in several places, therefore we extract this error case as its own function

function throwEmpty() {
  throw new Error("List is empty");
}

// Testing for equality is done element wise, a custom `test` parameter can be provided, to determine equality the list's elements

function eq(a, b, eqEl = (x, y) => x === y) {
  return a(
    () =>
      b(
        () => true,
        () => false
      ),
    (af, ar) =>
      b(
        () => false,
        (bf, br) => eqEl(af, bf) && eq(ar, br, eqEl)
      )
  );
}

// `find` elevates a predicate to a function, taking a list, and returning the sub-list that starts with the first element satisfying the predicate. If no such element exists an empty list is returned
//
// ```
// const l = cons(1, cons(5, cons('the', cons('rest'))))
// const fiveAndTheRest = find(x => x === 5)(l)
// ```

function find(predicate) {
  return function findIt(list) {
    return list(
      () => list,
      (first, rest) => (predicate(first) ? list : findIt(rest))
    );
  };
}

// ## Manipulation
//
// Reduce takes a `function (acc, el)` which shall `return res`, and _promotes_ it to a `function` on a list.
// The given list is reduced down, element-wise (visited as `el`), from _left to right_, according to the given function.
// An initial value can be given as the second parameter (either to `reduce` itself, or to the promoted function)
//
// ```
// const sum = reduce((acc, el) => acc + el, 0)
// const mySum = sum(myList)
// ```

function reduce(reducer, defaultInitial) {
  return function promoted(list, initial = defaultInitial) {
    return list(
      () => initial,
      (first, rest) => promoted(rest, reducer(initial, first))
    );
  };
}

// There is an dual version, of `reduce` which accumulates from _right to left_ instead

function reduceRight(reducer, defaultInitial) {
  return function promoted(list, initial = defaultInitial) {
    return list(
      () => initial,
      (first, rest) => reducer(promoted(rest, initial), first)
    );
  };
}

// The length of a list is just reducing it to a number by counting

const length = reduce((x) => x + 1, 0);

// Reversing a list, is just reducing the list to another list, simply `cons`-ing the elements from left to right

const reverse = reduce((acc, el) => cons(el, acc), empty);

// Appending two lists, is just reducing with another list as initial value
//
// ```
// const full = append(left, right)
// ```

const append = reduceRight((acc, el) => cons(el, acc));

// Mapping an element-transforming function to a whole-list-processing function is now easily expressed in terms of `reduceRight` (to retain the original order)
//
// ```
// const myIncrementedList = map(x => x + 1)(myList)
// ```

function map(mapping) {
  return reduceRight((acc, el) => cons(mapping(el), acc), empty);
}

// Creating a whole-list filter from an element-predicate is done in a similar fashion
//
// ```
// const myEvenList = filter(x => x % 2 === 0)(myList)
// ```

function filter(predicate) {
  return reduceRight((acc, el) => (predicate(el) ? cons(el, acc) : acc), empty);
}

// A function which transform an element to a whole list, can be _flat-mapped_ to a function processing a whole list itself:
// For each element of the list, the result is spliced into the list.
//
// ```
// const dup = x => cons(x, cons(x))
// const myDuplications = flatMap(dup)(myList)
// ```

function flatMap(mapping) {
  return reduceRight((acc, el) => append(mapping(el), acc), empty);
}

// The public interface of a list

module.exports = {
  empty,
  cons,
  isEmpty,
  first,
  rest,
  eq,
  find,
  reduce,
  reduceRight,
  length,
  reverse,
  append,
  map,
  filter,
  flatMap,
};
