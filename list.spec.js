const { expect } = require("chai");
const fc = require("fast-check");
const {
  empty,
  cons,
  isEmpty,
  first,
  rest,
  eq,
  find,
  reduce,
  reduceRight,
  length,
  reverse,
  append,
  map,
  filter,
  flatMap,
} = require("./list");

describe("inspirations in form of unit tests", () => {
  const satEmpty = decorate(isEmpty, "empty");
  const satEqSelf = decorate((e) => eq(e, e), "equal to itself");
  const satEq = (ref) => decorate((e) => eq(e, ref), `equal to ${ref}`);

  const myList = cons(1, cons(2));

  it("a list should know if its empty", () => {
    expect(empty).to.satisfy(satEmpty);
    expect(myList).to.not.satisfy(satEmpty);
  });

  it("a list should know its first", () => {
    expect(() => first(empty)).to.throw();
    expect(first(myList)).to.equal(1);
  });

  it("a list should know its rest", () => {
    expect(() => rest(empty)).to.throw();
    expect(rest(myList)).to.satisfy(satEq(cons(2)));
  });

  it("a list is equal to itself", () => {
    expect(empty).to.satisfy(satEqSelf);
    expect(empty).to.not.satisfy(satEq(myList));
    expect(myList).to.satisfy(satEqSelf);
    expect(myList).to.not.satisfy(satEq(empty));
  });

  it(`a list being reduced`, () => {
    const sum = reduce((acc, el) => acc + el, 0);
    expect(sum(empty)).to.be.equal(0);
    expect(sum(myList)).to.be.equal(3);
  });

  it(`a list being right-reduced`, () => {
    const sum = reduceRight((acc, el) => acc + el, 0);
    expect(sum(empty)).to.be.equal(0);
    expect(sum(myList)).to.be.equal(3);
  });

  it(`a list being of length`, () => {
    expect(length(empty)).to.be.equal(0);
    expect(length(myList)).to.be.equal(2);
  });

  it(`a list being reversed`, () => {
    expect(reverse(empty)).to.satisfy(satEq(empty));
    expect(reverse(myList)).to.satisfy(satEq(cons(2, cons(1))));
  });

  it(`lists being appended`, () => {
    expect(append(empty, empty)).to.satisfy(satEq(empty));
    expect(append(myList, empty)).to.satisfy(satEq(myList));
    expect(append(empty, myList)).to.satisfy(satEq(myList));
    expect(append(myList, myList)).to.satisfy(satEq(cons(1, cons(2, myList))));
  });

  it(`a list being searched`, () => {
    const it = (what) => (x) => x === what;
    expect(find(it(1))(empty)).to.satisfy(satEmpty);
    expect(find(it(1))(myList)).to.satisfy(satEq(myList));
    expect(find(it(2))(myList)).to.satisfy(satEq(cons(2)));
    expect(find(it(3))(myList)).to.satisfy(satEmpty);
  });

  it(`a list being mapped`, () => {
    const id = map((x) => x);

    expect(id(empty)).to.satisfy(satEq(empty));
    expect(id(myList)).to.satisfy(satEq(myList));

    const three = map((x) => 3);

    expect(three(empty)).to.satisfy(satEq(empty));
    expect(three(myList)).to.satisfy(satEq(cons(3, cons(3))));
  });

  it(`a list being filtered`, () => {
    const yes = filter((x) => true);

    expect(yes(empty)).to.satisfy(satEq(empty));
    expect(yes(myList)).to.satisfy(satEq(myList));

    const no = filter((x) => false);

    expect(no(empty)).to.satisfy(satEq(empty));
    expect(no(myList)).to.satisfy(satEq(empty));

    const even = filter((x) => x % 2 === 0);

    expect(even(empty)).to.satisfy(satEq(empty));
    expect(even(myList)).to.satisfy(satEq(cons(2)));
  });

  it(`a list being flat-mapped`, () => {
    const id = flatMap(cons);

    expect(id(empty)).to.satisfy(satEq(empty));
    expect(id(myList)).to.satisfy(satEq(myList));

    const three = flatMap((x) => cons(x, cons(x, cons(x))));

    expect(three(empty)).to.satisfy(satEq(empty));
    expect(three(myList)).to.satisfy(
      satEq(cons(1, cons(1, cons(1, cons(2, cons(2, cons(2)))))))
    );
  });
});

function decorate(fn, msg) {
  fn.toString = () => ` being ${msg}`;
  fn.inspect = () => ` being ${msg}`;
  return fn;
}

// arbitrary list of `el`s.
// e.g. arbitraryList(fc.integer()) for an arbitrary list of integers
function arbitraryList(el = fc.string()) {
  return fc.letrec((tie) => ({
    list: fc.oneof(tie("empty"), tie("consed"), tie("consed"), tie("consed")),
    consed: fc.tuple(tie("element"), tie("list")).map(([e, l]) => cons(e, l)),
    empty: fc.constant(empty),
    element: el,
  })).list;
}

// run tests with `npx mocha list.spec.js`
