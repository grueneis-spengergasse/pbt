// to run this file
// 1. install latest node (e.g. by using `scoop`)
// 2. cd <directory of code>
// 3. npm ci
// 4. npm run test

const { expect } = require("chai");

const fc = require("fast-check");

// code under test

const { myPlus, fizzBuzz } = require("./index");

// test suite

describe("All Tests", function () {
  it("Should calculate + correctly", function () {
    expect(myPlus(2, 2)).to.be.equal(4);
  });

  it("+ is commutative", function () {
    fc.assert(
      fc.property(fc.nat(), fc.nat(), (a, b) => myPlus(a, b) === myPlus(b, a))
    );
  });

  const divisibleByThree = fc.integer(1, 100).filter((i) => i % 3 === 0);
  it("Every third element starts with 'Fizz'", function () {
    fc.assert(
      fc.property(divisibleByThree, (i) => fizzBuzz()[i - 1].startsWith("Fizz"))
    );
  });
});
