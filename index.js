// to run this file
// 1. install latest node (e.g. by using `scoop`)
// 2. cd <directory of code>
// 3. npm ci
// 4. npm run test

// set this to `false` to _fix_ the code
const breakCode = true;

function myPlus(a = 0, b = 0, c = 0) {
  return (breakCode && a === 12 ? 13 : a) + b + c;
}

function fizzBuzz() {
  return Object.keys(new Array(100).fill())
    .filter((i) => +i !== 0)
    .map((i) => {
      const divBy3 = i % 3 === 0;
      const divBy5 = i % 5 === 0;

      return divBy3 && divBy5
        ? "FizzBuzz"
        : divBy3
        ? "Fizz"
        : divBy5
        ? "Buzz"
        : `${i}`;
    });
}

module.exports = { myPlus, fizzBuzz };
