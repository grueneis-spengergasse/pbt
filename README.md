
# Property based testing mit fast-check

<https://github.com/dubzzz/fast-check>

## How to run

1. install latest `node` (e.g. by using `scoop`)
2. `cd <directory of code>`
3. `npm ci`
4. `npm run test`
